package server;

/**
 * Created by zt on 2016/6/29.
 */

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;

/**
 * 
 */
class MyIoHandler extends IoHandlerAdapter {

    /**
     *
     * @param session
     * @param msg
     * @throws Exception
     */
    @Override
    public void messageReceived(IoSession session, Object msg)
            throws Exception {
        String str = msg.toString();
        System.out.println( "client["+session.getRemoteAddress().toString()+"] say : " + str    );
        if (str.endsWith("exit")) {
            session.close(true);
            return;
        }else{

            if(str.contains("who")){
                session.write("I am super robot .who are you ?");
            }else if(str.contains("where")){
                session.write("I am in you brain .where are you ?");
            }else if(str.contains("love")){
                session.write("I love you too.you are in my heart all time");
            }else if(str.contains("hate")){
                session.write("I still love you...");
            }else {
                int i = Integer.parseInt(Math.round(Math.random() * 11) + "");
                session.write(getAnswer(i));
            }
            System.out.println("对方正在输入...");
        }
    }

    public String getAnswer(int i){

        String str[]={
                "i am chinese",
                "who are you too",
                "on my way",
                "what are you saying?",
                "good idea",
                "i can not understand what you say",
                "maybe it works",
                "i wasn't born yesterday",
                "you are beautiful,you know that",
                "so what",
                "whatif"
        };
        return str[i];
    }
}
