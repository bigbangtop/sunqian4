package server;

import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;

/**
 * Created by zt on 2016/6/29.
 */
public class MinaServer {
    public static void main(String[] args) throws IOException {
        System.out.println("============server listen: 6666==============");
        IoAcceptor acceptor = new NioSocketAcceptor();
        acceptor.getSessionConfig().setReadBufferSize(1024 * 5);
        acceptor.getSessionConfig().setBothIdleTime(8);

        acceptor.getFilterChain().addLast("codec",
                new ProtocolCodecFilter(new TextLineCodecFactory(Charset.forName("UTF-8") )));
        acceptor.setHandler(new MyIoHandler());

        acceptor.bind(new InetSocketAddress(6666));
    }



}
