package client;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 * Created by zt on 2016/6/29.
 */
public class MyClientHandler extends IoHandlerAdapter {


    private final String openValue;

    public MyClientHandler(String openValue){
        this.openValue=openValue;
    }

    @Override
    public void sessionOpened(IoSession session) {
        session.write(openValue);
    }

    @Override
    public void messageReceived(IoSession session, Object message) throws Exception {
        System.out.println("  the server say   :" + message.toString());
        session.write(getStringFromStandardInput());
    }

    private   String getStringFromStandardInput() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str = null;
        try {
            System.out.println("输入请...");
            str = br.readLine();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }

}
